package Contest;

// Konstruktor InvalidInputException menerima pesan kesalahan sebagai parameter dan memanggil konstruktor superclass untuk mengatur pesan kesalahan.

class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
}