package Contest;

class CAT1 extends TiketKonser {
    // mengimplementasikan konstruktor yang menginisialisasi atribut namaTiket dan hargaTiket dari superclass TiketKonser. Konstruktor ini digunakan untuk menciptakan objek tiket dengan nama dan harga yang spesifik sesuai dengan jenis tiketnya.

    //Do your magic here...

    public CAT1(String nama,double harga) {
        this.namaTiket = nama;
        this.hargaTiket = harga;
    }
}