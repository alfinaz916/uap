package Contest;

class CAT8 extends TiketKonser {
    // mengimplementasikan konstruktor yang menginisialisasi atribut namaTiket dan hargaTiket dari superclass TiketKonser. Konstruktor ini digunakan untuk menciptakan objek tiket dengan nama dan harga yang spesifik sesuai dengan jenis tiketnya.

    //Do your magic here...

    public CAT8(String nama,double harga) {
        this.namaTiket = nama;
        this.hargaTiket = harga;
    }
}