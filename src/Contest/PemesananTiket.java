package Contest;

import java.util.Scanner;

class PemesananTiket {
    // Do your magic here...

    static String namaPemesan;

    static TiketKonser dipesan;
    
    //meminta input dari pengguna berupa nama pemesan dan pilihan jenis tiket. Jika input tidak valid, akan dilempar InvalidInputException.
    public static void run() throws InvalidInputException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan nama pemesan : ");
        namaPemesan = sc.nextLine();
        if (namaPemesan.length() >= 10) {
            throw new InvalidInputException("Nama pemesan harus kurang dari 10 karakter");
        }
        System.out.print("Pilih jenis tiket :");
        System.out.println("\n1. CAT8");
        System.out.println("2. CAT1");
        System.out.println("3. FESTIVAL");
        System.out.println("4. VIP");
        System.out.println("5. UNLIMITED EXPERIENCE");
        System.out.println("masukkan pilihan :");
        int pilihan = sc.nextInt();
        if (pilihan < 1 || pilihan > 5) {
            throw new InvalidInputException("pilihan tiket harus antara 1 hingga 5");
        }

        switch (pilihan) {
            case 1:
                dipesan = new CAT8("CAT8", 100);
                break;
            case 2:
                dipesan = new CAT1("CAT1", 1000000);
                break;
            case 3:
                dipesan = new FESTIVAL("FESTIVAL", 1100000);
                break;
            case 4:
                dipesan = new VIP("VIP", 1200000);
                break;
            case 5:
                dipesan = new VVIP("UNLIMITED EXPERIENCE", 1400000);
                break;
            default:
                break;
        }

        


        
    }
}