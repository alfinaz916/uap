//kelas abstrak yang mengimplementasikan interface HargaTiket. 

package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
     String namaTiket;
     double hargaTiket;

    @Override
    public double getHarga() {  //mengembalikan harga tiket yang sesuai.
        return this.hargaTiket;
    }
}