package Contest;

interface HargaTiket {
    //Interface ini memiliki satu metode getHarga() yang harus diimplementasikan oleh kelas-kelas yang mengimplementasikannya. Metode ini mengembalikan harga tiket yang sesuai.
    
    //Do your magic here...
    double getHarga();
}